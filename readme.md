
## CMake
总结一些用法  


- [ ] cmake的写法适配到Qt5,qt_add_library是6.2版本添加的
- [ ] 拷库目录Debug目前是hard coded的，改成动态的
- [ ] sentry恢复成能使用，使用源码编译也可以
- [ ] 跨平台编译测试
- [ ] Debug断点，launch.json弄一个
- [ ] Windeployqt总是搞不对dll，默认总是32位的


### 环境变量设置后有坑
只要设置了环境变量，windeployqt之后就只会拷贝默认环境变量的dll，配置里的就不会生效
