cmake_minimum_required(VERSION 3.16)
project(Resources VERSION 1.0 LANGUAGES C CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

# Set up AUTOMOC and some sensible defaults for runtime execution
# When using Qt 6.3, you can replace the code block below with
# qt_standard_project_setup()
set(CMAKE_AUTOMOC ON)
include(GNUInstallDirs)

find_package(QT NAMES Qt5 Qt6 REQUIRED COMPONENTS Core)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Gui)
find_package(Qt${QT_VERSION_MAJOR} OPTIONAL_COMPONENTS Widgets)


add_library(Resources
    Resources.cpp Resources.h
    Resources_global.h
)
target_compile_definitions(Resources PUBLIC
    RESOURCES_LIBRARY
)

target_link_libraries(Resources PUBLIC
    Qt::Core
)


# Resources:
set(QmlResources_resource_files
    "SecondQuickView.qml"
)

qt_add_resources(Resources "QmlResources"
    PREFIX
        ""
    FILES
        ${QmlResources_resource_files}
)

install(TARGETS Resources
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    FRAMEWORK DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)
