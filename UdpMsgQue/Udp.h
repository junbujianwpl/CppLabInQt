#ifndef UDP_H
#define UDP_H

#include <string>

using std::string;

class Udp
{
public:
    Udp(string locIp,int locPort,string dstIp,int dstPort);
    virtual ~Udp();

    int sendTo(string dstIp, int dstPort, char *data, int byteLen);

    int recvFrom(string locIp,int locPort,char* buf,int byteLen);


    string getErrTip() const{return m_strErrTip_;}

protected:

    string  m_strDstIp_;
    string  m_strLocIp_;

    int     m_nDstPort_;
    int     m_nLocPort_;

    int     m_fdSock_;

    string  m_strErrTip_;


private:
    void   formatTip(char* format,...);
};

#endif // UDP_H
