#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <iostream>

#include "Udp.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnExit_clicked()
{
    exit(0);
}

void MainWindow::on_btnUdpSend_clicked()
{
    char buf[256]={1};
    Udp u("",3333,"127.0.0.1",4444);

    u.sendTo("127.0.0.1",4444,buf,sizeof(buf));

    std::cout<<u.getErrTip().c_str()<<std::endl;

}

void MainWindow::on_btnUdpRecv_clicked()
{
    char buf[256]={1};
    Udp u("",3333,"127.0.0.1",4444);

    u.recvFrom("127.0.0.1",4444,buf,sizeof(buf));

    std::cout<<u.getErrTip().c_str()<<std::endl;

}
