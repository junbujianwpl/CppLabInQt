#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btnExit_clicked();

    void on_btnUdpSend_clicked();

    void on_btnUdpRecv_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
