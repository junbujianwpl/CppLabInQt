set(QT5_32_DIR C:/Qt/5.15.1/msvc2019)
set(QT5_64_DIR C:/Qt/5.15.1/msvc2019_64)
set(QT6_64_DIR C:/Qt/6.6.2/msvc2019_64)

# 忽略系统环境变量，因为默认的qt路径可能与指定的x64/32有冲突
set(ENV{PATH} "")

set(QT5_32_CONFIG_DIR "${QT5_32_DIR}/lib/cmake/Qt5")
set(QT5_64_CONFIG_DIR "${QT5_64_DIR}/lib/cmake/Qt5")

# set(QT_DIR ${QT5_64_CONFIG_DIR})
# set(QT5_DIR ${QT5_64_CONFIG_DIR})
if(CMAKE_SIZEOF_VOID_P EQUAL 8)
    # 设置64位编译时使用的变量
    set(ARCH "64")

    set(CMAKE_PREFIX_PATH ${QT5_64_DIR})
    set(QTDIR ${QT5_64_DIR})

    message(STATUS "Building for 64-bit target ${QTDIR}")
elseif(CMAKE_SIZEOF_VOID_P EQUAL 4)
    set(ARCH "32")

    set(CMAKE_PREFIX_PATH ${QT5_32_DIR})
    set(QTDIR ${QT5_32_DIR})

    message(STATUS "Building for 32-bit target ${QTDIR}")
else()
    message(WARNING "Unknown architecture, neither 32-bit nor 64-bit")
endif()

# 调用qtenv2.bat来设置环境变量
execute_process(
    COMMAND cmd /c "${QTDIR}/bin/qtenv2.bat"
    RESULT_VARIABLE result
)

# 检查命令执行是否成功
if(result)
    message(FATAL_ERROR "Failed to run qtenv2.bat: ${result}")
endif()


# 查找vcvarsall.bat的路径
# find_program(VCVARSALL_PATH vcvarsall.bat HINTS "C:/Program Files (x86)/Microsoft Visual Studio/2019/Community/VC/Auxiliary/Build/")

# if(NOT VCVARSALL_PATH)
#     message(FATAL_ERROR "vcvarsall.bat not found")
# endif()

# # 创建一个自定义目标来调用vcvarsall.bat
# add_custom_target(SetupVCVars ALL
#     COMMAND cmd /c "\"${VCVARSALL_PATH}\" x64 && echo VC environment setup"
#     COMMENT "Setting up VC environment..."
# )
