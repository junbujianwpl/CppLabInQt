#ifndef CMDEXC_H
#define CMDEXC_H

#include <string>


class CmdExc
{
public:
    CmdExc(std::string cmd,std::string mode="rt");
    virtual ~CmdExc();

    std::string getOutput() const;

private:
    std::string m_strOutput__;
    FILE* m_fp__;
};

#endif // CMDEXC_H
