#ifndef IPDEALER_H
#define IPDEALER_H

#include <string>
#include <vector>
#ifdef Q_OS_Windows
#include <winsock.h>
#endif

class IpDealer {
public:
  IpDealer();
  virtual ~IpDealer();

  std::string getTip() const;

  std::vector<std::string> getIpList();

  std::vector<std::string> getAdptInfo();

private:
  std::string m_strTips__; ///< tips
};

#endif // IPDEALER_H
