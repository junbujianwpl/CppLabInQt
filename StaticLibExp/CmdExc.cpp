#include "CmdExc.h"
CmdExc::CmdExc(std::string cmd, std::string mode) {
#ifdef Q_OS_Windows
  m_fp__ = _popen(cmd.c_str(), mode.c_str());
  char buf[256] = {0};
  if (NULL != m_fp__) {
    int read_len;
    while ((read_len = fread(buf, sizeof(buf) - 1, 1, m_fp__)) > 0) {
      m_strOutput__ += buf;
      memset(buf, 0, sizeof(buf));
    }
  }
#endif
}

CmdExc::~CmdExc() {
  if (NULL != m_fp__) {
#ifdef Q_OS_Windows
    _pclose(m_fp__);
#endif
  }
}

std::string CmdExc::getOutput() const { return m_strOutput__; }
