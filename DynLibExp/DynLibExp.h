#ifndef DYNLIBEXP_H
#define DYNLIBEXP_H

#include "dynlibexp_global.h"

class DYNLIBEXPSHARED_EXPORT DynLibExp
{

public:
    DynLibExp();
    static int mul(int a,int b);
};

#endif // DYNLIBEXP_H
