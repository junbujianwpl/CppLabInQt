﻿#include "WidgetFactory.h"
#include "ui_WidgetFactory.h"
#include <QDebug>
#include <QWidget>
#include <QWindow>

WidgetFactory::WidgetFactory(QWidget *parent)
    : QWidget(parent), ui(new Ui::WidgetFactory) {
  ui->setupUi(this);
}

WidgetFactory::~WidgetFactory() { delete ui; }

void WidgetFactory::on_btnCreateWidget_clicked() {
  auto modal = ui->checkBoxModal->checkState() == Qt::CheckState::Checked;
  auto setParent =
      ui->checkBoxSetParent->checkState() == Qt::CheckState::Checked;
  auto windowFlag =
      ui->checkBoxWindowFlag->checkState() == Qt::CheckState::Checked;
  auto w = new QWidget(setParent ? this : nullptr);
  w->setStyleSheet("background:red");
  if (modal) {
    w->setWindowModality(Qt::WindowModality::WindowModal);
  }
  qInfo() << "XXXXX modal" << modal;
  if (windowFlag) {
    w->setWindowFlags(w->windowFlags() | Qt::Window);
  }
  w->show();
}
