﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

private slots:
  void on_btnLibExp_clicked();

  void on_btnGetIp_clicked();

  void on_btnCmdExc_clicked();

  void on_btnThread_clicked();

  void on_btnOpenWidgetFactory_clicked();

  void on_btnOpenQuick_clicked();

  private:
  Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
