#ifndef THREADDEMO_H
#define THREADDEMO_H

#include <QObject>
#include <QThread>

enum EnFruit { apple, peach };
Q_DECLARE_METATYPE(EnFruit);

class Worker : public QObject {

  Q_OBJECT
public:
  Worker(QObject *parent = nullptr);

  void doWork(EnFruit e);
};
class ThreadController : public QObject {
  Q_OBJECT
public:
  ThreadController(QObject *parent = nullptr);
  ~ThreadController();

signals:
  void sigTriggerJob(EnFruit);

private:
  QThread thread;
  Worker worker;
};

#endif // THREADDEMO_H
