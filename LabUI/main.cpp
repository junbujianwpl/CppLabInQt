﻿#include "MainWindow.h"
#include <QApplication>
#include <QDebug>
#include <QString>
#include <QSysInfo>

// #include "sentry.h"

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);
  MainWindow w;
  w.show();

//   sentry_options_t *options = sentry_options_new();
//   sentry_options_set_backend(options, NULL);
//   sentry_options_set_dsn(
//       options, "http://902c5f95048542c4aaf33b9c32951d1a@192.168.0.101:9000/2");
//   qInfo() << "init return %d" << sentry_init(options);

//   sentry_value_t user = sentry_value_new_object();
//   sentry_value_set_by_key(
//       user, "id",
//       sentry_value_new_string(QString("%1").arg(2).toStdString().c_str()));
//   sentry_value_set_by_key(user, "ip_address",
//                           sentry_value_new_string("{{auto}}"));
//   sentry_value_set_by_key(user, "email",
//                           sentry_value_new_string("jane.doe@example.com"));
//   sentry_set_user(user);
//   sentry_capture_event(sentry_value_new_message_event(
//       /*   level */ SENTRY_LEVEL_INFO,
//       /*  logger */ "custom",
//       /* message */
//       QString("It works! from %1")
//           .arg(QSysInfo::productType())
//           .toStdString()
//           .c_str()));
  return a.exec();
}
