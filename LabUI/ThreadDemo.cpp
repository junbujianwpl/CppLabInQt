#include "ThreadDemo.h"
#include <QDebug>

ThreadController::ThreadController(QObject *parent) : QObject(parent) {

  connect(this, &ThreadController::sigTriggerJob, &worker, &Worker::doWork);
  worker.moveToThread(&thread);
  thread.start();
}

ThreadController::~ThreadController() {
  thread.quit();
  thread.wait();
}

Worker::Worker(QObject *parent) : QObject(parent) {
  qRegisterMetaType<EnFruit>();
}

void Worker::doWork(EnFruit e) {
  qInfo() << "I like friut" << int(e);
  qInfo() << "thread" << QThread::currentThread();
}
