﻿#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <iostream>

#include "../DynLibExp/DynLibExp.h"
#include "../StaticLibExp/CmdExc.h"
#include "../StaticLibExp/IpDealer.h"
#include "../StaticLibExp/StaticLibExp.h"
#include "Miscellaneous.h"
#include "ThreadDemo.h"
#include "WidgetFactory.h"
#include <QDebug>
#include <QString>
#include <View/FirstQuickView.h>
using namespace std;
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::on_btnLibExp_clicked() {

  std::cout << DynLibExp::mul(3, 4) << std::endl;
  std::cout << StaticLibExp::add(3, 4) << std::endl;
}

void MainWindow::on_btnGetIp_clicked() {
  IpDealer ip;
  ip.getIpList();
  cout << ip.getTip().c_str() << endl;
  vector<string> ips = ip.getAdptInfo();
  for (int i = 0; i < ips.size(); i++) {
    cout << ips[i].c_str() << endl;
    qDebug() << QString::fromStdString(ips[i]);
  }
}

void MainWindow::on_btnCmdExc_clicked() {
  //    CmdExc cmd("ipconfig");
  //    cout<<cmd.getOutput().c_str()<<endl;

  Miscellaneous m;
  m.testOperatorPriority();
}

void MainWindow::on_btnThread_clicked() {

  qInfo() << "main thread" << QThread::currentThread();
  ThreadController *t = new ThreadController;
  emit t->sigTriggerJob(EnFruit::apple);
}

void MainWindow::on_btnOpenWidgetFactory_clicked() {
  auto w = new WidgetFactory();
  w->show();
}

void MainWindow::on_btnOpenQuick_clicked()
{
  auto w=new FirstQuickView(this);
  w->show();
}

