﻿#ifndef FIRSTQUICKVIEW_H
#define FIRSTQUICKVIEW_H

#include <QQuickWidget>
#include <QDialog>
#include <QLayout>


class FirstQuickView:public QDialog
{
public:
    FirstQuickView(QWidget *parent=nullptr);

protected:
    QHBoxLayout *m_rootHLayout=nullptr;
};

#endif // FIRSTQUICKVIEW_H
