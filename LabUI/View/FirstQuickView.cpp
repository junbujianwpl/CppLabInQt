﻿#include "FirstQuickView.h"

FirstQuickView::FirstQuickView(QWidget* parent)
    :QDialog(parent)
{
    m_rootHLayout=new QHBoxLayout(this);
    setLayout(m_rootHLayout);
    QQuickWidget *qmlWidget = new QQuickWidget(this);
qmlWidget->setResizeMode(QQuickWidget::SizeRootObjectToView);
//qmlWidget->setSource(QUrl("file:../../../../CppLabInQt/LabUI/View/FirstQuickView.qml")); // 请根据你的文件路径调整
qmlWidget->setSource(QUrl("qrc:/SecondQuickView.qml")); // 请根据你的文件路径调整
m_rootHLayout->addWidget(qmlWidget);

}
