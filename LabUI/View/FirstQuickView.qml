﻿import QtQuick 2.15

Item {
    Rectangle{
        width: 100
        height: 100
        Text {
            id: name
            text: qsTr("text")
            color: 'green'
        }
        radius: 10
        border.color: 'red'
        border.width: 1
        antialiasing: true
    }

}
