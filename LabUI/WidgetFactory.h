﻿#ifndef WIDGETFACTORY_H
#define WIDGETFACTORY_H

#include <QWidget>

namespace Ui {
class WidgetFactory;
}

class WidgetFactory : public QWidget {
  Q_OBJECT

public:
  explicit WidgetFactory(QWidget *parent = nullptr);
  ~WidgetFactory();

private slots:
  void on_btnCreateWidget_clicked();

private:
  Ui::WidgetFactory *ui;
};

#endif // WIDGETFACTORY_H
